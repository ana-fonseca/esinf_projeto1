package Model;

import java.util.Set;

public class Estacao {

    private String descricao;
    private Set<Bilhete> entrada;
    private Set<Bilhete> saida;
    private String zona;
    private int nEstacao;

    public Estacao(int nEstacao, String descricao, String zona) {
        this.descricao = descricao;
        this.zona = zona;
        this.nEstacao = nEstacao;
    }

    public int getnEstacao(){
        return nEstacao;
    }

    public String getZona() {
        return zona;
    }

}
