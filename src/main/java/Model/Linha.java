package Model;

import java.util.*;

/**
 *
 * @author DEI-ISEP
 * @param <Estacao> Generic list element type
 */
public class Linha<Estacao> implements Iterable<Estacao>, Cloneable {

// instance variables of the DoublyLinkedList
    private final Node<Estacao> header;     // header sentinel
    private final Node<Estacao> trailer;    // trailer sentinel
    private int size = 0;       // number of elements in the list
    private int modCount = 0;   // number of modifications to the list (adds or removes)
    public List<Viagem> listaViagens = new ArrayList<>();

    /**
     * Creates both elements which act as sentinels
     */
    public Linha() {

        header = new Node<>(null, null, null);      // create header
        trailer = new Node<>(null, header, null);   // trailer is preceded by header
        header.setNext(trailer);                    // header is followed by trailer
    }

    /**
     * Returns the number of elements in the linked list
     *
     * @return the number of elements in the linked list
     */
    public int size() {
        return size;
    }

    /**
     * Retorna o indíce da estação recebida se pertencer à DoublyLinkedList Linha, senão, lança uma exceção
     *
     * @param estacao - estação para ser encontrada
     * @return o índice da estação em questão
     */
    public int getIndex(Model.Estacao estacao) throws Exception {
        ListIterator<Model.Estacao> list = (ListIterator<Model.Estacao>) this.listIterator();
        while(list.hasNext()){
            if (list.next().getnEstacao() == estacao.getnEstacao()){
                return list.nextIndex() - 1;
            }
        }
        throw new Exception("That element is not store in this List");
    }

    /**
     * Retorna a estação que está guardada no índice recebido caso pertença à DoublyLinkedList Linha, senão, lança
     * uma exceção
     *
     * @param i - indíce onde procurar
     * @return a estação guardada no indíce em questão
     */
    public Model.Estacao get(int i) throws Exception {
        ListIterator<Model.Estacao> list = (ListIterator<Model.Estacao>) this.listIterator();
        while(list.hasNext()){
            if (list.nextIndex() == i){
                return list.next();
            }
            list.next();
        }
        throw new Exception("That element is not stored in this List");
    }

    /**
     * Checks if the list is empty
     *
     * @return true if the list is empty, and false otherwise
     */
    public boolean isEmpty() {
        return size == 0;
    }

    /**
     * Returns (but does not remove) the first element in the list
     *
     * @return the first element of the list
     */
    public Estacao first() {
        return header.getNext().getElement();
    }

    /**
     * Returns (but does not remove) the last element in the list
     *
     * @return the last element of the list
     */
    public Estacao last() {
        return trailer.getPrev().getElement();
    }

// public update methods
    /**
     * Adds an element e to the front of the list
     *
     * @param e element to be added to the front of the list
     */
    public void addFirst(Estacao e) {
        // place just after the header
        addBetween(e, header, header.getNext());

    }

    /**
     * Adds an element e to the end of the list
     *
     * @param e element to be added to the end of the list
     */
    public void addLast(Estacao e) {
        // place just before the trailer
        addBetween(e, trailer.getPrev(), trailer);
    }

    /**
     * Removes and returns the first element of the list
     *
     * @return the first element of the list
     */
    public Estacao removeFirst() {
        return remove(header.getNext());
    }

    /**
     * Removes and returns the last element of the list
     *
     * @return the last element of the list
     */
    public Estacao removeLast() {
        return remove(trailer.getPrev());
    }

// private update methods
    /**
     * Adds an element e to the linked list between the two given nodes.
     */
    private void addBetween(Estacao e, Node<Estacao> predecessor, Node<Estacao> successor) {
        Node newNode = new Node(e, predecessor, successor);
        predecessor.setNext(newNode);
        successor.setPrev(newNode);
        size++;
        modCount++;
    }

    /**
     * Removes a given node from the list and returns its content.
     */
    private Estacao remove(Node<Estacao> node) {
        if (!node.equals(trailer) && !node.equals(header)) {
            node.getNext().setPrev(node.getPrev());
            node.getPrev().setNext(node.getNext());
            size--;
            modCount++;
            return node.getElement();
        }
        return null;
    }

// Overriden methods
    @Override
    public boolean equals(Object obj) {
        if (getClass() != obj.getClass()) {
            return false;
        }
        Linha dllist = (Linha) obj;
        if (this.size() != dllist.size()) {
            return false;
        }
        LinhaIterator t = (LinhaIterator) listIterator();
        LinhaIterator n = (LinhaIterator) dllist.listIterator();
        while (t.hasNext() && n.hasNext()) {
            if (!t.next().equals(n.next())) {
                return false;
            }
        }
        return true;
    }

    @Override
    public Object clone() throws CloneNotSupportedException {
        Linha clone = new Linha();
        LinhaIterator it1 = (LinhaIterator) this.listIterator();
        LinhaIterator it2 = (LinhaIterator) clone.listIterator();
        while (it1.hasNext()) {
            it2.add(it1.next());
        }
        return clone;
    }

//---------------- nested DoublyLinkedListIterator class ----------------
    private class LinhaIterator implements ListIterator<Estacao> {

        private Linha.Node<Estacao> nextNode, prevNode, lastReturnedNode; // node that will be returned using next and prev respectively
        private int nextIndex;  // Index of the next element
        private int expectedModCount;  // Expected number of modifications = modCount;

        public LinhaIterator() {
            this.prevNode = header;
            this.nextNode = header.getNext();
            lastReturnedNode = null;
            nextIndex = 0;
            expectedModCount = modCount;
        }

        final void checkForComodification() {  // invalidate iterator on list modification outside the iterator
            if (modCount != expectedModCount) {
                throw new ConcurrentModificationException();
            }
        }

        @Override
        public boolean hasNext() {
            return nextNode != trailer;
        }

        @Override
        public Estacao next() throws NoSuchElementException {
            checkForComodification();
            if (hasNext()) {
                this.prevNode = nextNode;
                this.nextNode = nextNode.getNext();
                nextIndex++;
                lastReturnedNode = prevNode;
                return prevNode.getElement();
            } else {
                throw new NoSuchElementException("End of list reached.");
            }
        }

        @Override
        public boolean hasPrevious() {
            return prevNode != header;
        }

        @Override
        public Estacao previous() throws NoSuchElementException {
            checkForComodification();
            if (hasPrevious()) {
                this.nextNode = prevNode;
                this.prevNode = prevNode.getPrev();
                nextIndex--;
                lastReturnedNode = nextNode;
                return nextNode.getElement();
            } else {
                throw new NoSuchElementException("End of list reached.");
            }
        }

        @Override
        public int nextIndex() {
            return nextIndex;
        }

        @Override
        public int previousIndex() {
            return nextIndex - 1;
        }

        @Override
        public void remove() throws NoSuchElementException {
            checkForComodification();
            if (lastReturnedNode != null) {
                expectedModCount++;
                Linha.this.remove(lastReturnedNode);
                lastReturnedNode = null;
            } else {
                throw new NoSuchElementException();
            }
        }

        @Override
        public void set(Estacao e) throws NoSuchElementException {
            if (lastReturnedNode == null) {
                throw new NoSuchElementException();
            }
            checkForComodification();

            lastReturnedNode.setElement(e);
        }

        @Override
        public void add(Estacao e) {
            checkForComodification();
            Linha.this.addBetween(e, prevNode, nextNode);
            nextNode = prevNode.getNext();
            expectedModCount++;
            next();
        }

    }    //----------- end of inner DoublyLinkedListIterator class ----------

//---------------- Iterable implementation ----------------
    @Override
    public Iterator<Estacao> iterator() {
        return new LinhaIterator();
    }

    public ListIterator<Estacao> listIterator() {
        return new LinhaIterator();
    }

//---------------- nested Node class ----------------
    private static class Node<E> {

        private E element;      // reference to the element stored at this node
        private Node<E> prev;   // reference to the previous node in the list
        private Node<E> next;   // reference to the subsequent node in the list

        public Node(E element, Node<E> prev, Node<E> next) {
            this.element = element;
            this.prev = prev;
            this.next = next;
        }

        public E getElement() {
            return element;
        }

        public Node<E> getPrev() {
            return prev;
        }

        public Node<E> getNext() {
            return next;
        }

        public void setElement(E element) { // Not on the original interface. Added due to list iterator implementation
            this.element = element;
        }

        public void setPrev(Node<E> prev) {
            this.prev = prev;
        }

        public void setNext(Node<E> next) {
            this.next = next;
        }
    } //----------- end of nested Node class ----------

    /**
     * Retorna um Map em que regista, na primeira coluna, uma estação e, na segunda coluna, o número de passagens por
     * esta mesma.
     *
     * @return um Map que tem a lista das estação na primeira coluna e o número de passagens por cada uma delas na
     * segunda coluna
     */
    public HashMap<Model.Estacao, Integer> utilPEstacao() throws Exception {
        Integer num = 1;
        HashMap<Model.Estacao, Integer> stats = new HashMap<>();
        for(Viagem v: listaViagens){
            int indexOrigem = getIndex(v.getOrigem());
            int indexDestino = getIndex(v.getDestino());
            if(indexOrigem < indexDestino){
                for(int i = indexOrigem; i <= indexDestino; i++){
                    if (!stats.containsKey(get(i))){
                        stats.put(get(i), num);
                    } else {
                        stats.put(get(i), stats.get(get(i)) + num);
                    }
                }
            }else{
                for(int i = indexDestino; i < indexOrigem; i++){
                    if (!stats.containsKey(get(i))){
                        stats.put(get(i), num);
                    } else {
                        stats.put(get(i), stats.get(i) + num);
                    }
                }
            }
        }
        return stats;
    }

    /**
     * Recebendo uma viagem, regista-a na lista de viagens armazenada nesta classe
     */
    public void addViagem(Viagem viagem){

            listaViagens.add(viagem);

    }

    /**
     * Retorna os bilhetes que estão em transgressao de acordo com o número de zonas que podem passar e o número de zonas
     * pelas quais realmente passaram vendo as estações e o bilhete da viagem em questão.
     *
     * @return transgressoes = um Set<> que contém todos os bilhetes em transgressão
     */
    public Set<Bilhete> transgressoes() throws Exception {
        Set<Bilhete> transgressoes = new HashSet<>();
        for (Viagem viagem : listaViagens) {
            Set<String> zonas = new HashSet<>();
            int origemIndex = getIndex(viagem.getOrigem());
            int destinoIndex = getIndex(viagem.getDestino());
            if (origemIndex < destinoIndex){
                for (int i = origemIndex;i <= destinoIndex;i++){
                    zonas.add(get(i).getZona());
                }
            } else {
                for (int i = destinoIndex;i <= origemIndex;i++){
                    zonas.add(get(i).getZona());
                }
            }
            if (viagem.getBilhete().getNZonas() < zonas.size()){
                transgressoes.add(viagem.getBilhete());
            }
        }
        return transgressoes;
    }

    /**
     * Retorna a maior sequência de estações pela qual o bilhete recebido pode passar tendo em conta o número de zonas
     * pelas quais este pode passar e às zonas das estações ordenando estas pelo maior número de estações por zona
     *
     * @param bilhete - bilhete que deve ser utilizado para obter esta sequência em função do número de zonas permitido
     * @return um Set<> que contém a maior sequência de estações pela qual o bilhete em questão pode passar
     */
    public Set<Model.Estacao> maiorSequencia(Bilhete bilhete){
        Set<Model.Estacao> maiorSequencia = new HashSet<>();
        Map<Integer, Set<Model.Estacao>> nEstacoesPorZona = new TreeMap<>(
                new Comparator<Integer>() {
                    @Override
                    public int compare(Integer o1, Integer o2) {
                        return o2.compareTo(o1);//sort in descending order
                    }
                });
        Set<String> zonas = new HashSet<>();
        ListIterator<Model.Estacao> listEstacoes = (ListIterator<Model.Estacao>) this.listIterator();
        while(listEstacoes.hasNext()){
            Model.Estacao estacao = listEstacoes.next();
            zonas.add(estacao.getZona());
        }
        for (String zona : zonas){
            Set<Model.Estacao> estacoes = new HashSet<>();
            Iterator<Model.Estacao> itEstacoes = (Iterator<Model.Estacao>) this.iterator();
            while(itEstacoes.hasNext()){
                Model.Estacao estacao = itEstacoes.next();
                if (zona.equals(estacao.getZona())){
                    estacoes.add(estacao);
                }
            }
            nEstacoesPorZona.put(estacoes.size(), estacoes);
        }
        int i = 0;
        Iterator<Integer> it = nEstacoesPorZona.keySet().iterator();
        while(i < bilhete.getNZonas() && it.hasNext()){
            maiorSequencia.addAll(nEstacoesPorZona.get(it.next()));
            i++;
        }

        return maiorSequencia;
    }
}