package Model;

public class Bilhete {

    private int num;
    private String tipo;

    public Bilhete(int num, String tipo) {
        this.num = num;
        this.tipo = tipo;
    }

    public int getNum() {
        return num;
    }

    public String getTipo() {
        return tipo;
    }

    //This method returns the number of Zones that the ticket has.
    public int getNZonas(){
        String temp;
        temp = this.getTipo().substring(1); // Splits the string in half to get the number.
        return Integer.parseInt(temp);  // Transforms the string we got into an integer
    }

}
