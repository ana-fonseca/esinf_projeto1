package Model;

public class Viagem {

    private Bilhete bilhete;
    private Estacao origem;
    private Estacao destino;

    public Viagem(Bilhete bilhete, Estacao origem, Estacao destino) {
        this.bilhete = bilhete;
        this.origem = origem;
        this.destino = destino;
    }

    public Estacao getOrigem() {
        return origem;
    }

    public Estacao getDestino() {
        return destino;
    }

    public Bilhete getBilhete() {
        return bilhete;
    }

}
