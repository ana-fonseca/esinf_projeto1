package UnitTests;

import Model.Bilhete;
import Model.Estacao;
import Model.Viagem;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotSame;

public class ViagemTest {

    @org.junit.Test
    public void getOrigem() {
        System.out.println("getOrigem");
        Estacao origem = new Estacao(10, "Casa da Música", "Z3");
        Estacao destino = new Estacao(9, "Francos", "Z3");
        Bilhete bilhete = new Bilhete(111222333, "Z2");
        Viagem v = new Viagem(bilhete, origem, destino);
        Estacao expResult = origem;
        Estacao result = v.getOrigem();
        assertEquals(expResult, result);
    }

    @org.junit.Test
    public void getOrigemFalse() {
        System.out.println("getOrigemFalse");
        Estacao origem = new Estacao(10, "Casa da Música", "Z3");
        Estacao destino = new Estacao(9, "Francos", "Z3");
        Bilhete bilhete = new Bilhete(111222333, "Z2");
        Viagem v = new Viagem(bilhete, origem, destino);
        Estacao expResult = destino;
        Estacao result = v.getOrigem();
        assertNotSame(expResult, result);
    }


    @org.junit.Test
    public void getDestino() {
        System.out.println("getDestino");
        Estacao origem = new Estacao(10, "Casa da Música", "Z3");
        Estacao destino = new Estacao(9, "Francos", "Z3");
        Bilhete bilhete = new Bilhete(111222333, "Z2");
        Viagem v = new Viagem(bilhete, origem, destino);
        Estacao expResult = destino;
        Estacao result = v.getDestino();
        assertEquals(expResult, result);
    }

    @org.junit.Test
    public void getDestinoFalse() {
        System.out.println("getDestinoFalse");
        Estacao origem = new Estacao(10, "Casa da Música", "Z3");
        Estacao destino = new Estacao(9, "Francos", "Z3");
        Bilhete bilhete = new Bilhete(111222333, "Z2");
        Viagem v = new Viagem(bilhete, origem, destino);
        Estacao expResult = origem;
        Estacao result = v.getDestino();
        assertNotSame(expResult, result);
    }



    @org.junit.Test
    public void getBilhete() {
        System.out.println("getBilhete");
        Estacao origem = new Estacao(10, "Casa da Música", "Z3");
        Estacao destino = new Estacao(9, "Francos", "Z3");
        Bilhete bilhete = new Bilhete(111222333, "Z2");
        Viagem v = new Viagem(bilhete, origem, destino);
        Bilhete expResult = bilhete;
        Bilhete result = v.getBilhete();
        assertEquals(expResult, result);
    }

    @org.junit.Test
    public void getBilheteFalse() {
        System.out.println("getBilheteFalse");
        Estacao origem = new Estacao(10, "Casa da Música", "Z3");
        Estacao destino = new Estacao(9, "Francos", "Z3");
        Bilhete bilhete = new Bilhete(111222333, "Z2");
        Bilhete bilhete1 = new Bilhete(123456789, "Z2");
        Viagem v = new Viagem(bilhete, origem, destino);
        Bilhete expResult = bilhete1;
        Bilhete result = v.getBilhete();
        assertNotSame(expResult, result);
    }

}