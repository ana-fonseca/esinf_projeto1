package UnitTests;

import Model.Bilhete;
import Model.Estacao;
import Model.Linha;
import Model.Viagem;
import org.junit.Test;

import java.util.*;

import static junit.framework.TestCase.assertEquals;
import static junit.framework.TestCase.assertTrue;
import static org.junit.Assert.assertFalse;

public class LinhaTest {
    

        @Test
        public void testSize() {
            System.out.println("size");
            Linha instance = new Linha();
            assertTrue("result should be zero", (instance.size()==0));
            instance.addFirst(null);
            assertTrue("result should be one", (instance.size()==1));
            instance.addLast(null);
            assertTrue("result should be two", (instance.size()==2));
            instance.removeFirst();
            assertTrue("result should be one", (instance.size()==1));
            instance.removeLast();
            assertTrue("result should be zero", (instance.size()==0));
        }

        /**
         * Test of isEmpty method, of class Linha.
         */
        @Test
        public void testIsEmpty() {
            System.out.println("isEmpty");
            Linha instance = new Linha();

            assertTrue("result should be yes", (instance.isEmpty()==true));
            instance.addFirst(null);
            assertTrue("result should be no", (instance.isEmpty()==false));
            instance.addLast(null);
            assertTrue("result should be no", (instance.isEmpty()==false));
            instance.removeFirst();
            assertTrue("result should be no", (instance.isEmpty()==false));
            instance.removeLast();
            assertTrue("result should be yes", (instance.isEmpty()==true));
        }

        /**
         * Test of first method, of class Linha.
         */
        @Test
        public void testFirst() {
            System.out.println("first");
            Linha <String> instance = new Linha<>();

            assertTrue("result should be null", (instance.first()==null));
            instance.addFirst("Xpto");
            assertTrue("result should be Xpto", (instance.first().compareTo("Xpto")==0));
            instance.addLast("Ypto");
            assertTrue("result should be Xpto", (instance.first().compareTo("Xpto")==0));
            instance.removeFirst();
            assertTrue("result should be Ypto", (instance.first().compareTo("Ypto")==0));
            instance.removeLast();
            assertTrue("result should be null", (instance.first()==null));
        }

        /**
         * Test of last method, of class Linha.
         */
        @Test
        public void testLast() {
            System.out.println("last");
            Linha <String> instance = new Linha <>();

            assertTrue("result should be null", (instance.last()==null));
            instance.addFirst("Xpto");
            assertTrue("result should be Xpto", (instance.last().compareTo("Xpto")==0));
            instance.addLast("Ypto");
            assertTrue("result should be Ypto", (instance.last().compareTo("Ypto")==0));
            instance.removeLast();
            assertTrue("result should be Xpto", (instance.last().compareTo("Xpto")==0));
            instance.removeFirst();
            assertTrue("result should be null", (instance.last()==null));
        }

        /**
         * Test of addFirst method, of class Linha.
         */
        @Test
        public void testAddFirst() {
            System.out.println("addFirst");
            Linha <String> instance = new Linha <>();

            instance.addFirst("Xpto");
            assertTrue("result should be Xpto", (instance.first().compareTo("Xpto")==0));
            instance.addFirst("Ypto");
            assertTrue("result should be Ypto", (instance.first().compareTo("Ypto")==0));
            instance.addFirst("Zpto");
            assertTrue("result should be Zpto", (instance.first().compareTo("Zpto")==0));
        }

        /**
         * Test of addLast method, of class Linha.
         */
        @Test
        public void testAddLast() {
            System.out.println("addLast");
            Linha <String> instance = new Linha <>();

            instance.addLast("Xpto");
            assertTrue("result should be Xpto", (instance.last().compareTo("Xpto")==0));
            instance.addLast("Ypto");
            assertTrue("result should be Ypto", (instance.last().compareTo("Ypto")==0));
            instance.addLast("Zpto");
            assertTrue("result should be Zpto", (instance.last().compareTo("Zpto")==0));
        }

        /**
         * Test of removeFirst method, of class Linha.
         */
        @Test
        public void testRemoveFirst() {
            System.out.println("removeFirst");
            Linha <String> instance = new Linha <>();

            instance.addFirst("Xpto");
            instance.addFirst("Ypto");
            instance.addFirst("Zpto");

            assertTrue("result should be Zpto", (instance.removeFirst().compareTo("Zpto")==0));
            assertTrue("result should be Ypto", (instance.removeFirst().compareTo("Ypto")==0));
            assertTrue("result should be Xpto", (instance.removeFirst().compareTo("Xpto")==0));
            assertTrue("result should be null", (instance.removeFirst()==null));
        }

        /**
         * Test of removeLast method, of class Linha.
         */
        @Test
        public void testRemoveLast() {
            System.out.println("removeLast");
            Linha <String> instance = new Linha <>();

            instance.addLast("Xpto");
            instance.addLast("Ypto");
            instance.addLast("Zpto");

            assertTrue("result should be Zpto", (instance.removeLast().compareTo("Zpto")==0));
            assertTrue("result should be Ypto", (instance.removeLast().compareTo("Ypto")==0));
            assertTrue("result should be Xpto", (instance.removeLast().compareTo("Xpto")==0));
            assertTrue("result should be null", (instance.removeLast()==null));
        }


        /**
         * Test of overridden equals method, of class Linha.
         */
        @Test
        public void testEquals() {
            System.out.println("equals");
            Linha <String> instance1 = new Linha <>(), instance2 = new Linha <>();

            instance1.addLast("Xpto");
            instance1.addLast("Ypto");
            instance1.addLast("Zpto");

            instance2.addLast("Xpto");
            assertFalse("Lists should not be equal", (instance1.equals(instance2)));
            assertFalse("Lists should not be equal", (instance2.equals(instance1)));
            instance2.addLast("Ypto");
            assertFalse("Lists should not be equal", (instance1.equals(instance2)));
            assertFalse("Lists should not be equal", (instance2.equals(instance1)));
            instance2.addLast("Zpto");
            assertTrue("Lists should be equal", (instance1.equals(instance2)));
            assertTrue("Lists should be equal", (instance2.equals(instance1)));
        }
        /**
         * Test of overridden equals method, of class Linha.
         * @throws java.lang.CloneNotSupportedException
         */
        @Test
        public void testClone() throws CloneNotSupportedException {
            System.out.println("clone");
            Linha<String> instance1 = new Linha<>(), instance2;

            instance1.addLast("Xpto");
            instance1.addLast("Ypto");
            instance1.addLast("Zpto");

            instance2 = (Linha<String>) instance1.clone();

            assertTrue("Lists should be of equal size", (instance1.size() == instance2.size()));
            Iterator<String> it1 = instance1.iterator();       // ITerator for This list
            Iterator<String> it2 = instance2.iterator();      // ITerator for the Other list
            while (it1.hasNext()) {
                String el1 = it1.next(), el2 = it2.next();
                assertTrue("Elements should be the equal", (el1.equals(el2)));
            }

            // check deep structure
            instance2.removeLast();
            instance2.removeLast();
            instance2.removeLast();

            it1 = instance1.iterator();       // ITerator for This list
            Integer total = 0;
            while (it1.hasNext()) {
                it1.next();
                total++;
            }
            assertTrue("List should have remained the same", (total == 3));
        }


    @org.junit.Test
    public void getIndex() throws Exception {
        Linha<Estacao> linha = new Linha<>();
        Estacao e1 = new Estacao(10, "Casa da Música", "C2");
        Estacao e2 = new Estacao(9, "Francos", "C2");
        linha.addFirst(e1);
        linha.addLast(e2);
        int i = linha.getIndex(e2);
        int expected = 2;
        assertEquals(expected, i);

    }

    @org.junit.Test
    public void utilPEstacao() throws Exception {
        Linha<Estacao> linha = new Linha<>();
        Estacao e1 = new Estacao(10, "Casa da Música", "C3");
        Estacao e2 = new Estacao(9, "Francos", "C1");
        Estacao e3 = new Estacao(3, "7 Bicos", "C3");
        Estacao e4 = new Estacao(6, "Matosinhos", "C2");
        Bilhete bilhete1 = new Bilhete(111222333, "Z3");
        Bilhete bilhete2 = new Bilhete(112222233, "Z2");
        Bilhete bilhete3 = new Bilhete(111233333, "Z2");
        linha.addLast(e1);
        linha.addLast(e3);
        linha.addLast(e4);
        linha.addLast(e2);
        Viagem v1 = new Viagem(bilhete1, e1, e2);
        Viagem v2 = new Viagem(bilhete2, e1, e3);
        Viagem v3 = new Viagem(bilhete3, e3, e4);
        linha.addViagem(v1);
        linha.addViagem(v2);
        linha.addViagem(v3);

        Map<Estacao, Integer> expected = new HashMap<>();
        expected.put(e1, 2);
        expected.put(e2, 1);
        expected.put(e3, 3);
        expected.put(e4, 2);
        assertEquals(expected, linha.utilPEstacao());
    }

    @org.junit.Test
    public void addViagem() {
        Linha<String> linha = new Linha<>();
        Estacao origem = new Estacao(10, "Casa da Música", "Z3");
        Estacao destino = new Estacao(9, "Francos", "Z3");
        Bilhete bilhete = new Bilhete(111222333, "Z2");
        Viagem v = new Viagem(bilhete, origem, destino);
        linha.addViagem(v);
        assertTrue(linha.listaViagens.contains(v));
    }

    @Test
    public void maiorsequencia() throws Exception {

        Linha<Model.Estacao> linha = new Linha<>();
        Estacao e1 = new Estacao(10, "Casa da Música", "C2");
        Estacao e2 = new Estacao(9, "Francos", "C2");
        Estacao e3 = new Estacao(8, "Lapa", "C2");
        Estacao e4 = new Estacao(7, "Trindade", "C1");
        Estacao e5 = new Estacao(6, "Campanhã", "C1");
        Estacao e6 = new Estacao(5, "Estádio do Dragão", "C3");
        linha.addLast(e1);
        linha.addLast(e4);
        linha.addLast(e2);
        linha.addLast(e5);
        linha.addLast(e3);
        linha.addLast(e6);
        Bilhete bilhete = new Bilhete(111222333, "Z2");

        Set<Estacao> maiorSequencia = new HashSet<>();
        maiorSequencia.add(e1);
        maiorSequencia.add(e2);
        maiorSequencia.add(e3);
        maiorSequencia.add(e4);
        maiorSequencia.add(e5);
        assertEquals(maiorSequencia, linha.maiorSequencia(bilhete));
    }

    @org.junit.Test
    public void transgressoes() throws Exception {

        Linha<Estacao> linha = new Linha<>();
        Estacao e1 = new Estacao(10, "Casa da Música", "C3");
        Estacao e2 = new Estacao(9, "Francos", "C1");
        Estacao e3 = new Estacao(3, "7 Bicos", "C3");
        Estacao e4 = new Estacao(6, "Matosinhos", "C2");
        Bilhete bilhete1 = new Bilhete(111222333, "Z3");
        Bilhete bilhete2 = new Bilhete(112222233, "Z2");
        Bilhete bilhete3 = new Bilhete(111233333, "Z2");
        linha.addLast(e1);
        linha.addLast(e3);
        linha.addLast(e4);
        linha.addLast(e2);
        Viagem v1 = new Viagem(bilhete1, e1, e2);
        Viagem v2 = new Viagem(bilhete2, e3, e2);
        Viagem v3 = new Viagem(bilhete3, e3, e2);
        linha.addViagem(v1);
        linha.addViagem(v2);
        linha.addViagem(v3);

        Set<Bilhete> transgressoes = new HashSet<>();
        transgressoes.add(bilhete2);
        transgressoes.add(bilhete3);

        assertEquals(transgressoes, linha.transgressoes());

    }
}