package UnitTests;

import Model.Estacao;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotSame;

public class EstacaoTest {

    @org.junit.Test
    public void getnEstacao() {
        System.out.println("getNum");
        Estacao estacao = new Estacao(10, "Casa da Música", "Z3");
        int expResult = 10;
        int result = estacao.getnEstacao();
        assertEquals(expResult, result);
    }

    @org.junit.Test
    public void getnEstacaoFalse() {
        System.out.println("getnEstacaoFalse");
        Estacao estacao = new Estacao(10, "Casa da Música", "Z3");
        int expResult = 11;
        int result = estacao.getnEstacao();
        assertNotSame(expResult, result);
    }


    @org.junit.Test
    public void getZona() {
        System.out.println("getZona");
        Estacao estacao = new Estacao(10, "Casa da Música", "Z3");
        String expResult = "Z3";
        String result = estacao.getZona();
        assertEquals(expResult, result);
    }

    @org.junit.Test
    public void getZonaFalse() {
        System.out.println("getZonaFalse");
        Estacao estacao = new Estacao(10, "Casa da Música", "Z3");
        String expResult = "Z5";
        String result = estacao.getZona();
        assertNotSame(expResult, result);
    }
}