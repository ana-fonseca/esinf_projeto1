package UnitTests;

import Model.Bilhete;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotSame;

public class BilheteTest {

    @Test
    public void getNum() {
        System.out.println("getNum");
        Bilhete instance = new Bilhete(111222333, "Z2");
        int expResult = 111222333;
        int result = instance.getNum();
        assertEquals(expResult, result);
    }

    @Test
    public void getNumFalse(){
        System.out.println("getNumFalse");
        Bilhete instance = new Bilhete(111222333, "Z2");
        int expResult = 000000000;
        int result = instance.getNum();
        assertNotSame(expResult, result);
    }

    @org.junit.Test
    public void getTipo() {
        System.out.println("getTipo");
        Bilhete instance = new Bilhete(111222333, "Z2");
        String expResult = "Z2";
        String result = instance.getTipo();
        assertEquals(expResult, result);
    }

    @Test
    public void getTipoFalse(){
        System.out.println("getTipoFalse");
        Bilhete instance = new Bilhete(111222333, "Z2");
        String expResult = "Z4";
        String result = instance.getTipo();
        assertNotSame(expResult, result);

    }


    @org.junit.Test
    public void getNZonas() {
        System.out.println("getNZonas");
        Bilhete instance = new Bilhete(111222333, "Z2");
        int expResult = 2;
        int result = instance.getNZonas();
        assertEquals(expResult, result);
    }

    @Test
    public void getNZonasFalse() {
        System.out.println("getNZonasFalse");
        Bilhete instance = new Bilhete(111222333, "Z2");
        int expResult = 3;
        int result = instance.getNZonas();
        assertNotSame(expResult, result);
    }
}