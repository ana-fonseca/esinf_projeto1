package Utils;

import Model.*;
import java.io.File;
import java.io.FileNotFoundException;
import java.util.*;

public class FileManager {
    
    private static String ESTACOES_FILENAME = "fx_estacoes.txt";
    private static String VIAGENS_FILENAME = "fx_viagens.txt";
    private Linha linha = new Linha();

    /**
     * Recebendo um ficheiro, verifica se este é o ficheiro "estações" ou o ficheiro "viagens", se for então transfere
     * este ficheiro para o seu respetivo método privado e returna true, senão retorna false
     *
     * @param file - o ficheiro para ser analisado
     * @return true se o ficheiro for válido, false se não for
     */
    public boolean readFile(File file) throws Exception {
        if (file.getName().equals(ESTACOES_FILENAME)){
            readEstacoes(file);
            return true;
        } else if (file.getName().equals(VIAGENS_FILENAME) && linha.size() > 0){
            readViagens(file);
            return true;
        } 
        return false;
    }

    /**
     * Recebe o ficheiro "estacoes" e lê-o criando estações e adiciona-as à DoublyLinkedList Linha
     *
     * @param fileEstacoes = ficheiro "estacoes" que contém as estações a serem criadas
     */
    private void readEstacoes(File fileEstacoes) throws FileNotFoundException{
        Scanner scFile = new Scanner(fileEstacoes);
        while(scFile.hasNextLine()){
            String[] line = scFile.nextLine().trim().split(",");
            linha.addLast(new Estacao(Integer.parseInt(line[0]), line[1], line[2]));
        }
    }

    /**
     * Recebe o ficheiro "viagens" e lê-o se já existirem estações e cria viagens criando bilhetes também e adicionando
     * estas viagens à lista armazenada na DoublyLinkedList Linha, armazenando o bilhete, a estação origem e a estação
     * destino
     *
     * @param fileViagens = file "viagens" that contains the viagens to be created
     */
    private void readViagens(File fileViagens) throws Exception {
        Scanner scFile = new Scanner(fileViagens);
        if (linha.size() > 0){
            while(scFile.hasNextLine()){
                String[] line = scFile.nextLine().trim().split(",");
                Bilhete newBilhete = new Bilhete (Integer.parseInt(line[0]), line[1]);
                Estacao origem = null;
                Estacao destino = null;
                while (linha.iterator().hasNext()){
                    Estacao next = ((Estacao) linha.iterator().next());
                    if (next.getnEstacao() == Integer.parseInt(line[2])){
                        origem = next;
                    } else if (next.getnEstacao() == Integer.parseInt(line[3])){
                        destino = next;
                    }
                }
                linha.addViagem(new Viagem(newBilhete, origem, destino));
            }
        } else {
            throw new Exception("Não existem ainda estações inseridas");
        }
    }

    public Linha getLinha() {
        return linha;
    }
}
